package com.example.notfoundtester;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.mymodule.mylib.BlankFragment;

/**
 * 
 */
public class FailPagerAdapter extends FragmentPagerAdapter {


    public FailPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        return new BlankFragment();
    }

    @Override
    public int getCount() {
        return 1;
    }
}
