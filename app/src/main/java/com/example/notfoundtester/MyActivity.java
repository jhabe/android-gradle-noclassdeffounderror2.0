package com.example.notfoundtester;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;


public class MyActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        ViewPager pager = (ViewPager) findViewById(R.id.viewpager_gallery);
        PagerAdapter adapter = new FailPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
    }

}
